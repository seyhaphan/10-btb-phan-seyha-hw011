import React, { Component } from 'react'
import { Card, Form, Container, Row } from 'react-bootstrap';
import ResultList from './components/ResultList';

import './style.css';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sign: '+',
            value1: '',
            value2: '',
            results: []
        };
    }

    handleChange = event => this.setState({ [event.target.name]: event.target.value })
    onAdd = () => {
        const { value1, value2, sign } = this.state;

        if (!value1 || !value2) {
            alert("can't calculate");
        }
        else if (isNaN(value1) || isNaN(value2)) {
            alert("Can't calculate number!");
        }
        else {
            let result;
            switch (sign) {
                case '+':
                    result = Number(value1) + Number(value2);
                    break;
                case '-':
                    result = Number(value1) - Number(value2);
                    break;
                case '*':
                    result = Number(value1) * Number(value2);
                    break
                case '/':
                    result = Number(value1) / Number(value2);
                    break;
                case '%':
                    result = Number(value1) % Number(value2);
                    break;
            }
            // this.setState({ results: [...this.state.results,result]});
            this.setState({
                results: [...this.state.results, {
                    result: result,
                    value1: this.state.value1,
                    value2: this.state.value2,
                    sign: this.state.sign
                }]
            });
        }

    }
    render() {
        let logo = "https://purepng.com/public/uploads/large/purepng.com-calculator-icon-android-lollipopsymbolsiconsgooglegoogle-iconsandroid-lollipoplollipop-iconsandroid-50-721522597141natii.png";

        return (
            <Container>
                <Row>
                    <div className="col-md-3">
                        <Card >
                            <Card.Img variant="top" src={logo} />
                            <Card.Body>
                                <Form.Control
                                    className='child'
                                    type="text"
                                    name="value1"
                                    value={this.state.value1}
                                    onChange={this.handleChange} />
                                <Form.Control
                                    className='child'
                                    type="text"
                                    name='value2'
                                    value={this.state.value2}
                                    onChange={this.handleChange} />
                                <Form.Control
                                    className='child'
                                    as="select"
                                    name="sign"
                                    onChange={this.handleChange}>
                                    <option value='+'>+ Plus</option>
                                    <option value='-'>- Subtract</option>
                                    <option value='*'>* Multiply</option>
                                    <option value='/'>/ Devide</option>
                                    <option value='%'>% Modulo</option>
                                </Form.Control>
                                <button
                                    className='btn btn-primary'
                                    onClick={this.onAdd}>Calculate</button>
                            </Card.Body>
                        </Card>
                    </div>
                    <div className="col-md-3">
                        <ResultList
                            results={this.state.results}
                        />
                    </div>
                </Row>
            </Container>
        );
    }
}
