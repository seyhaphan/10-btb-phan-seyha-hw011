import React from 'react'
import { ListGroup } from 'react-bootstrap';

export default function ResultItem(props) {
    const { result, value1, value2, sign } = props.result;
    return <ListGroup.Item> ({value1} {sign} {value2}) = {result} </ListGroup.Item>
}