import React from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import ResultItem from './ResultItem';

const style = {
   margin: 0,
   textAlign: 'center'
}
export default function ResultList(props) {
   return (
      <Card>
         <Card.Header>
            <h4 style={style}>Result History</h4>
         </Card.Header>
         <ListGroup variant="flush">
            {
               props.results.map((result, index) =>
                  <ResultItem
                     key={index}
                     result={result}
                  />
               )
            }
         </ListGroup>
      </Card>
   );
}
